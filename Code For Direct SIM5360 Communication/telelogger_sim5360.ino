/******************************************************************************
* Reference sketch for a vehicle telematics data feed for Freematics Hub
* Works with Freematics ONE/ONE+ with SIM5360 3G module
* Developed by Stanley Huang https://www.facebook.com/stanleyhuangyc
* Distributed under BSD license
* Visit http://freematics.com/hub/api for Freematics Hub API reference
* Visit http://freematics.com/products/freematics-one for hardware information
* To obtain your Freematics Hub server key, contact support@freematics.com.au
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
******************************************************************************/

#include <FreematicsONE.h>
#include "config.h"
#include "datalogger.h"

// logger states
#define STATE_SD_READY 0x1
#define STATE_OBD_READY 0x2
#define STATE_GPS_READY 0x4
#define STATE_MEMS_READY 0x8
#define STATE_NET_READY 0x10
#define STATE_CONNECTED 0x20
#define STATE_ALL_GOOD 0x40

#define EVENT_LOGIN 1
#define EVENT_LOGOUT 2

//Global Variables
#if USE_MEMS
byte accCount = 0; // count of accelerometer readings
long accSum[3] = {0}; // sum of accelerometer data
int accCal[3] = {0}; // calibrated reference accelerometer data
byte deviceTemp = 0; // device temperature
#endif
int lastSpeed = 0;
uint32_t lastSpeedTime = 0;
uint32_t distance = 0;
uint32_t startTick = 0;

char udpIP[16] = {0};
uint16_t udpPort = SERVER_PORT;

class COBD3G : public COBDSPI {
public:
    //Public Variables
    char buffer[128];

    COBD3G() {
        buffer[0] = 0;
    }

    bool netInit() {
        for (byte n = 0; n < 10; n++) {
            // try turning on module
            xbTogglePower();
            sleep(3000);
            // discard any stale data
            xbPurge();
            for (byte m = 0; m < 3; m++) {
                if (netSendCommand("AT\r")) {
                    return true;
                }
            }
        }
        return false;
    }

    bool netSetup(const char *apn, bool only3G = false) {
        uint32_t t = millis();
        bool success = false;
        netSendCommand("ATE0\r");
        netSendCommand("AT+CNAO=2\r");
        if (only3G) {
            netSendCommand("AT+CNMP=14\r"); // use WCDMA only
        }
        do {
            do {
                Serial.print('.');
                sleep(500);
                success = netSendCommand("AT+CPSI?\r", 1000, "Online");
                if (success) {
                    if (!strstr_P(buffer, PSTR("NO SERVICE"))) {
                        break;
                    }
                    success = false;
                } else {
                    if (strstr_P(buffer, PSTR("Off"))) {
                        break;
                    }
                }
            } while (millis() - t < 60000);
            if (!success) {
                break;
            }

            t = millis();
            do {
                success = netSendCommand("AT+CREG?\r", 5000, "+CREG: 0,1");
            } while (!success && millis() - t < 30000);
            if (!success) {
                break;
            }

            do {
                success = netSendCommand("AT+CGREG?\r", 1000, "+CGREG: 0,1");
            } while (!success && millis() - t < 30000);
            if (!success) {
                break;
            }

            do {
                sprintf_P(buffer, PSTR("AT+CGSOCKCONT=1,\"IP\",\"%s\"\r"), apn);
                success = netSendCommand(buffer);
            } while (!success && millis() - t < 30000);
            if (!success) {
                break;
            }

            success = netSendCommand("AT+CSOCKSETPN=1\r");
            if (!success) {
                break;
            }

            success = netSendCommand("AT+CIPMODE=0\r");
            if (!success) {
                break;
            }

            netSendCommand("AT+NETOPEN\r");
        } while (0);
        if (!success) {
            Serial.println(buffer);
        }
        return success;
    }

    const char *getIP() {
        uint32_t t = millis();
        char *ip = 0;
        do {
            if (netSendCommand("AT+IPADDR\r", 5000, "\r\nOK\r\n", true)) {
                char *p = strstr(buffer, "+IPADDR:");
                if (p) {
                    ip = p + 9;
                    if (*ip != '0') {
                        break;
                    }
                }
            }
            sleep(500);
            ip = 0;
        } while (millis() - t < 15000);
        return ip;
    }

    int getSignal() {
        if (netSendCommand("AT+CSQ\r", 500)) {
            char *p = strchr(buffer, ':');
            if (p) {
                p += 2;
                int db = atoi(p) * 10;
                p = strchr(p, '.');
                if (p) {
                    db += *(p + 1) - '0';
                }
                return db;
            }
        }
        return -1;
    }

    char *getOperatorName() {
        // display operator name
        if (netSendCommand("AT+COPS?\r") == 1) {
            char *p = strstr(buffer, ",\"");
            if (p) {
                p += 2;
                char *s = strchr(p, '\"');
                if (s) {
                    *s = 0;
                }
                return p;
            }
        }
        return 0;
    }

    bool udpOpen() {
        char *ip = queryIP(SERVER_URL);
        if (ip) {
            Serial.println(ip);
            strncpy(udpIP, ip, sizeof(udpIP) - 1);
        } else {
            return false;
        }
        sprintf_P(buffer, PSTR("AT+CIPOPEN=0,\"UDP\",\"%s\",%u,8000\r"), udpIP, udpPort);
        return netSendCommand(buffer, 3000);
    }

    void udpClose() {
        netSendCommand("AT+CIPCLOSE\r");
    }

    bool udpSend(const char *data, unsigned int len, bool wait = true) {
        sprintf_P(buffer, PSTR("AT+CIPSEND=0,%u,\"%s\",%u\r"), len, udpIP, udpPort);
        if (netSendCommand(buffer, 100, ">")) {
            xbWrite(data, len);
            if (!wait) {
                return true;
            }
            return waitCompletion(1000);
        } else {
            Serial.print("UDP data unsent ");
            Serial.println(buffer);
        }
        return false;
    }

    char *udpReceive(int *pbytes = 0) {
        if (netSendCommand(0, 3000, "+IPD")) {
            char *p = strstr(buffer, "+IPD");
            if (!p) {
                return 0;
            }
            int len = atoi(p + 4);
            if (pbytes) {
                *pbytes = len;
            }
            p = strchr(p, '\n');
            if (p) {
                *(++p + len) = 0;
                return p;
            }
        }
        return 0;
    }

    bool waitCompletion(int timeout) {
        return netSendCommand(0, timeout);
    }

    char *queryIP(const char *host) {
        sprintf_P(buffer, PSTR("AT+CDNSGIP=\"%s\"\r"), host);
        if (netSendCommand(buffer, 10000)) {
            char *p = strstr(buffer, host);
            if (p) {
                p = strstr(p, ",\"");
                if (p) {
                    char *ip = p + 2;
                    p = strchr(ip, '\"');
                    if (p) {
                        *p = 0;
                    }
                    return ip;
                }
            }
        }
        return 0;
    }

    bool netSendCommand(const char *cmd, unsigned int timeout = 2000, const char *expected = "\r\nOK\r\n",
                        bool terminated = false) {
        if (cmd) {
            xbWrite(cmd);
        }
        sleep(50);
        buffer[0] = 0;
        byte ret = xbReceive(buffer, sizeof(buffer), timeout, &expected, 1);
        if (ret) {
            if (terminated) {
                char *p = strstr(buffer, expected);
                if (p) {
                    *p = 0;
                }
            }
            return true;
        } else {
            return false;
        }
    }
};

//Global Variables
COBD3G sim;
String cmd;

void setup() {
    Serial.begin(115200);

    Serial.println("Start up");
    sim.begin();
    sim.xbBegin();
    sim.netInit();
    Serial.println("Net init complete");
}

void loop() {
    bool commandRead = false;
    while (!commandRead) {
        cmd = Serial.readString();
        if (cmd != "") {
          commandRead = true;
        }
    }

    sim.netSendCommand(const_cast<char*>(cmd.c_str()));

    Serial.println(sim.buffer);
    commandRead = false;
}
